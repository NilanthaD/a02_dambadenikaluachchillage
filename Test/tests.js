QUnit.test( "hello test", function( assert ) {
  assert.ok( 1 == "1", "Passed!" );
});

// QUnit.test( "First Test", function( assert ) {
//   assert.ok(Test.Celsias(),"true", 'return empty');
// });

QUnit.test( "Fahranhite to Celsias Test", function( assert ) {
  assert.strictEqual(Test.Celsias(32),0, '32F returns 0C');
  assert.strictEqual(Test.Celsias(""),'Empty Value', 'Returns Empty Value');
  assert.strictEqual(Test.Celsias("text"),'Not a Number', 'Returns Not a Number');
  assert.strictEqual(Test.Celsias(23),-5, 'Positive number returns correctly');
  assert.strictEqual(Test.Celsias(-20),-28.89, 'Negative number returns correctly');
});

QUnit.test( "Fahranhite to Kelvin Test", function( assert ) {
  assert.strictEqual(Test.Kelvin(-459.67),0, '-459.67F returns 0K');
  assert.strictEqual(Test.Kelvin(""),'Empty Value', 'Returns Empty Value');
  assert.strictEqual(Test.Kelvin("text"),'Not a Number', 'Returns Not a Number');
  assert.strictEqual(Test.Kelvin(124),324.26, 'Positive number returns correctly');
  assert.strictEqual(Test.Kelvin(-20),244.26, 'Negative number returns correctly');
});